#include "table.h"

#define TIME_BASE 			(10000)

static void _philo_thread_function(void* ptr)
{
	philo_t *pPhilo = (philo_t*)ptr;
	dbg_printf("Philo %d (%s) started (%u - %u)\n", pPhilo->pos, pPhilo->name, pPhilo->eat_time, pPhilo->sleep_time);

	if (!pPhilo->firstfork || !pPhilo->secondfork)
		return;

	/* Wait to start thread untill table releases the philosophers */
	pthread_mutex_lock(&pPhilo->table->kotteklein);
	pthread_mutex_unlock(&pPhilo->table->kotteklein);

	while(1)
	{
		/* Sleep */
		dbg_printf("Philo %d (%s) is sleeping %d\n", pPhilo->pos, pPhilo->name, pPhilo->sleep_time);
		usleep(pPhilo->sleep_time * TIME_BASE);

#ifdef ARBITRATOR
		/* Ask permission to the waiter */
		if(waiter_ask(table_getWaiter(pPhilo->table), pPhilo))
		{
			dbg_printf("Philo %d (%s), not able to ask the waiter\n", pPhilo->pos, pPhilo->name);
			return;
		}
		dbg_printf("Philo %d (%s), asked the waiter\n", pPhilo->pos, pPhilo->name);
#endif

		/* Take forks */
		if(fork_take(pPhilo->firstfork, pPhilo))
		{
			dbg_printf("Philo %d (%s), not able to take firstfork\n", pPhilo->pos, pPhilo->name);
			return;
		}
		dbg_printf("Philo %d (%s), took first fork (+%d)\n", pPhilo->pos, pPhilo->name, pPhilo->firstfork->pos);

#ifdef FORCE_DEADLOCK_SLEEPTIME
		/* Sleeping between taking the first and the second fork will produce a deadlock more 
		 * easily. All philos will take their first fork and not be able to take their second
		 * fork, as it is already taken by the other philosopher.
		 */
		usleep(FORCE_DEADLOCK_SLEEPTIME * TIME_BASE);
#endif

		if(fork_take(pPhilo->secondfork, pPhilo))
		{
			dbg_printf("Philo %d (%s), not able to take secondfork\n", pPhilo->pos, pPhilo->name);
			return;
		}
		dbg_printf("Philo %d (%s), took second fork (+%d)\n", pPhilo->pos, pPhilo->name, pPhilo->secondfork->pos);

#ifdef ARBITRATOR
		/* Permission to the waiter not needed anymore: release*/
		if(waiter_release(table_getWaiter(pPhilo->table), pPhilo))
		{
			dbg_printf("Philo %d (%s), not able to release the waiter\n", pPhilo->pos, pPhilo->name);
			return;
		}
		dbg_printf("Philo %d (%s), released the waiter\n", pPhilo->pos, pPhilo->name);
#endif

		/* Eat */
		dbg_printf("Philo %d (%s) is eating %d\n", pPhilo->pos, pPhilo->name, pPhilo->eat_time);
		usleep(pPhilo->eat_time * TIME_BASE);
		pPhilo->total_eaten += pPhilo->eat_time;

		/* Release forks */
		if(fork_release(pPhilo->secondfork, pPhilo))
		{
			dbg_printf("Philo %d (%s), not able to release secondfork\n", pPhilo->pos, pPhilo->name);
			return;
		}
		dbg_printf("Philo %d (%s), released second fork (-%d)\n", pPhilo->pos, pPhilo->name, pPhilo->secondfork->pos);
		if(fork_release(pPhilo->firstfork, pPhilo))
		{
			dbg_printf("Philo %d (%s), not able to release firsttfork\n", pPhilo->pos, pPhilo->name);
			return;
		}
		dbg_printf("Philo %d (%s), released first fork (-%d)\n", pPhilo->pos, pPhilo->name, pPhilo->firstfork->pos);
	}
	free(pPhilo);
}

philo_t* philo_create(table_t* table, unsigned int position, char* name, unsigned int eat_time, unsigned int sleep_time)
{
	philo_t* pPhilo = malloc(sizeof(philo_t));

	strcpy(pPhilo->name, name);
	pPhilo->table = table;
	pPhilo->pos = position;
	pPhilo->eat_time = eat_time;
	pPhilo->sleep_time = sleep_time;
	pPhilo->total_eaten = 0;
	pPhilo->firstfork = NULL;
	pPhilo->secondfork = NULL;
	return pPhilo;	
}

void philo_destroy(table_t* table, unsigned int position)
{
	philo_t* pPhilo = table_getPhilosopher(table, position);
	if (!pPhilo)
	{
		dbg_printf("Could not destroy Philo\n");
		return;
	}
	free(pPhilo);
}

void philo_bindforks(table_t* table, unsigned int position, fork_t* first, fork_t* second)
{
	philo_t* pPhilo = table_getPhilosopher(table, position);
	if (!pPhilo)
		return;
	pPhilo->firstfork = first;
	pPhilo->secondfork = second;
}

void philo_start(table_t* table, unsigned int position)
{
	philo_t* pPhilo = table_getPhilosopher(table, position);
	if (!pPhilo)
	{
		dbg_printf("Could not start Philo\n");
		return;
	}
	if (!pPhilo->firstfork || !pPhilo->secondfork)
	{
		dbg_printf("Philo %d (%s): no forks bound\n", pPhilo->pos, pPhilo->name);
		return;
	}
	pPhilo->total_eaten = 0;
	pthread_create(&pPhilo->thread, NULL, (void*)_philo_thread_function, (void*)pPhilo);
}

void philo_stop(table_t* table, unsigned int position)
{
	philo_t* pPhilo = table_getPhilosopher(table, position);
	if (!pPhilo)
	{
		dbg_printf("Could not stop Philo\n");
		return;
	}
	if(pthread_cancel(pPhilo->thread))
	{
		dbg_printf("Could not stop Philo thread\n");
	}
	fork_release(pPhilo->firstfork, pPhilo);
	fork_release(pPhilo->secondfork, pPhilo);
	printf("Stopped %s (%d): eaten %d\n", pPhilo->name, pPhilo->pos, pPhilo->total_eaten);
}

/** EOF **/

