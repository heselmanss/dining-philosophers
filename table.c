#include "table.h"

table_t* table_create(tablecfg_t* tcfg)
{
	unsigned i;
	table_t* t = malloc(sizeof(table_t));

	if(!t) return NULL;

	strcpy(t->name, tcfg->name);
	t->n_philos = 0;
	dbg_printf("Creating: %s\n", t->name);

	/* Create mutex on which all philosophers will wait before starting their threads */
	pthread_mutex_init(&t->kotteklein, NULL);
	pthread_mutex_lock(&t->kotteklein);

	/* Allocate the forks for the table */
	for(i=0; i<tcfg->n_philos; i++)
	{
		t->forks[i] = fork_create(t, i);
	}

	/* Configuration to real data structures */
	for(i=0; i<tcfg->n_philos; i++)
	{
		unsigned left = i;
		unsigned right = (i+1) % tcfg->n_philos;
		unsigned first = left;
		unsigned second = right;
#ifdef RESOURCE_HIERARCHY
		/* The forks will be numbered 0 through N and each philosopher will always pick up 
		 * the lower-numbered fork first, and then the higher-numbered fork, from among the
		 * two forks he plans to use. The order in which each philosopher puts down the 
		 * forks does not matter.
		 */
		first = MIN(left, right);
		second = MAX(left, right);
#endif
		t->philos[i] = philo_create(t, i, tcfg->philos[i].name, 
			tcfg->philos[i].eat_time, tcfg->philos[i].sleep_time);
		t->n_philos++;
		philo_bindforks(t, i, t->forks[first], t->forks[second]);
	}
#ifdef ARBITRATOR
	/* Create the waiter */
	t->waiter = waiter_create(t);
#endif
	return t;
}

void table_destroy(table_t* t)
{
	unsigned i;
	for(i=0; i<t->n_philos; i++)
	{
		fork_destroy(t, i);
		philo_destroy(t, i);
	}
}

void table_start(table_t* t)
{
	unsigned i;
	printf("Starting: %s\n", t->name);
	/* Start the philosophers */
	for(i=0; i<t->n_philos; i++)
	{
		philo_start(t, i);
	}
	/* Release all philosophers threads */
	pthread_mutex_unlock(&t->kotteklein);
}

void table_stop(table_t* t)
{
	unsigned i;
	dbg_printf("Stopping: %s\n", t->name);
	for(i=0; i<t->n_philos; i++)
	{
		philo_stop(t, i);
	}
}

philo_t* table_getPhilosopher(table_t* table, unsigned int position)
{
	if(!table)
		return NULL;
	if(position >= table->n_philos)
		return NULL;
	return table->philos[position];
}

fork_t* table_getFork(table_t* table, unsigned int position)
{
	if(!table)
		return NULL;
	if(position >= table->n_philos)
		return NULL;
	return table->forks[position];
}
waiter_t* table_getWaiter(table_t* table)
{
	if(!table)
		return NULL;
	return table->waiter;
}

/** EOF **/

