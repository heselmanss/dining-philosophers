#include "table.h"

fork_t* fork_create(table_t* table, unsigned int position)
{
	fork_t* f;
	if(!table)
		return NULL;
	f = malloc(sizeof(fork_t));
	f->table = table;
	f->by = NULL;
	f->pos = position;
	pthread_mutex_init(&f->taken, NULL);
	return f;
}

void fork_destroy(table_t* table, unsigned int position)
{
	fork_t* f = table_getFork(table, position);
	if (!f)
	{
		dbg_printf("Could not destroy fork\n");
		return;
	}
	pthread_mutex_destroy(&f->taken);
	free(f);
}

int fork_take(fork_t* f, philo_t* p)
{
	if(!f) return -1;
	pthread_mutex_lock(&f->taken);
	f->by = p;
	return 0;
}

int fork_release(fork_t* f, philo_t* p)
{
	if(!f) return -1;
	f->by = NULL;
	pthread_mutex_unlock(&f->taken);
	return 0;
}

/** EOF **/

