#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>

typedef struct 
{
	int thread_no;
	unsigned timeout;
	char message[100];
} thdata;

void thread_function(void* ptr)
{
	short i;
	thdata* pThreadInfo = (thdata*) ptr;
	for(i=0; i<10; i++)
	{
		printf("%s (%d, %s) %d\n", __FUNCTION__, pThreadInfo->thread_no, pThreadInfo->message, i);
		usleep(pThreadInfo->timeout*1000);
	}
	pthread_exit(0);
}

int main(int argc, char** argv)
{
	pthread_t thread1, thread2;
	thdata data1, data2;

	data1.thread_no = 1;
	strcpy(data1.message, "Hallo!");
	data1.timeout = 100;

	data2.thread_no = 2;
	strcpy(data2.message, "Hello!");
	data2.timeout = 80;

	pthread_create(&thread1, NULL, (void*)&thread_function, (void*)&data1);
	pthread_create(&thread2, NULL, (void*)&thread_function, (void*)&data2);

	pthread_join(thread1, NULL);
	pthread_join(thread2, NULL);

	return 0;
}

