#include "table.h"

waiter_t* waiter_create(table_t* table)
{
	waiter_t* w = malloc(sizeof(waiter_t));
	pthread_mutex_init(&w->serving, NULL);
	w->to = 0;
	dbg_printf("Waiter created\n");
	return w;
}

void waiter_destroy(table_t* table)
{
	waiter_t* w = table_getWaiter(table);
	if(!w)
	{
		dbg_printf("Could not destroy waiter\n");
		return;
	}
	pthread_mutex_destroy(&w->serving);
	free(w);
}

int waiter_ask(waiter_t* w, philo_t* philo)
{
	if(!w) return -1;
	pthread_mutex_lock(&w->serving);
	w->to = philo;
	return 0;
}

int waiter_release(waiter_t* w, philo_t* philo)
{
	if(!w) return -1;
	w->to = NULL;
	pthread_mutex_unlock(&w->serving);
	return 0;
}

/** EOF **/

