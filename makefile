
.PHONY: clean all howtopthread diner cscope

CC_FLAGS = -g -Werror -Wall 

#In order to get debug prints
#CC_FLAGS += -DDEBUG_PRINTS

#In order to force a deadlock on the forks
#CC_FLAGS += -DFORCE_DEADLOCK_SLEEPTIME=100

#Apply the Resource hierarchy solution
#CC_FLAGS += -DRESOURCE_HIERARCHY

#Apply the Arbitrator solution
#CC_FLAGS += -DARBITRATOR

all: diner cscope

howtopthread:
	gcc $(CC_FLAGS) howtopthread.c -o howtopthread -pthread -Werror -Wall
	./howtopthread

diner: diner.o philo.o table.o fork.o waiter.o makefile
	gcc $(CC_FLAGS) diner.o philo.o table.o fork.o waiter.o -pthread -o diner

%.o:%.c makefile table.h
	gcc $< -c $(CC_FLAGS) -pthread

cscope:
	find . -iname "*.c" -o -iname "*.h" > cscope.files
	cscope -bvuq -i cscope.files
	rm -f cscope.files

clean:
	rm -rf howtopthread diner *.o cscope*
