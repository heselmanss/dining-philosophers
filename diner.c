#include "table.h"

//#define RESTART_AFTER_STOP

int main(int argc, char** argv)
{
	table_t* the_table;
	tablecfg_t the_table_config =
	{
		"De beste tafel",
		6,
		{
			{"papabear",    30, 10},
			{"mamabear",    30, 10},
			{"babybear",    30, 10},
			{"sisterbear",  30, 10},
			{"brotherbear", 30, 10},
			{"neighbour",   30, 10}
		}
	};
	the_table = table_create(&the_table_config);
	table_start(the_table);
	sleep(10);
	table_stop(the_table);
#ifdef RESTART_AFTER_STOP
	sleep(3);
	table_start(the_table);
	sleep(10);
	table_stop(the_table);
#endif
	table_destroy(the_table);
	return 0;
}

/** EOF **/

