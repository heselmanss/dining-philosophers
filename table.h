#ifndef __TABLE_H__
#define __TABLE_H__

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>

#define MAX_PHILOSOPHERS		(10)
#define MAX_NAME			(100)

#define MIN(x,y) (((x) > (y)) ? (x) : (y))
#define MAX(x,y) (((x) < (y)) ? (x) : (y))

#ifdef DEBUG_PRINTS
#define dbg_printf(fmt, ...)  printf(fmt, ##__VA_ARGS__)
#else
#define dbg_printf(fmt, ...) 
#endif

typedef struct philocfg_s
{
	char* name;
	unsigned int eat_time;
	unsigned int sleep_time;
} philocfg_t;

typedef struct tablecfg_s
{
	char* name;
	unsigned int n_philos;
	philocfg_t philos[MAX_PHILOSOPHERS];
} tablecfg_t;

typedef struct table_s table_t;
typedef struct philo_s philo_t;
typedef struct fork_s fork_t;
typedef struct waiter_s waiter_t;

struct fork_s
{
	table_t* table;
	unsigned int pos;
	pthread_mutex_t taken;
	philo_t* by;
};

struct philo_s
{
	char name[MAX_NAME];
	table_t* table;
	unsigned int pos;
	unsigned int eat_time;
	unsigned int sleep_time;
	unsigned int total_eaten;
	pthread_t thread;
	fork_t* firstfork;
	fork_t* secondfork;
};

struct waiter_s
{
	table_t* table;
	pthread_mutex_t serving;
	philo_t* to;
};

struct table_s
{
	char name[MAX_NAME];
	unsigned int n_philos;
	philo_t* philos[MAX_PHILOSOPHERS];
	fork_t* forks[MAX_PHILOSOPHERS];
	waiter_t* waiter;
	pthread_mutex_t kotteklein;
};

fork_t* fork_create(table_t* table, unsigned int position);
void fork_destroy(table_t* table, unsigned int position);
int fork_take(fork_t* f, philo_t* p);
int fork_release(fork_t* f, philo_t* p);

philo_t* philo_create(table_t* table, unsigned int position, char* name, unsigned int eat_time, unsigned int sleep_time);
void philo_destroy(table_t* table, unsigned int position);
void philo_bindforks(table_t* table, unsigned int position, fork_t* first, fork_t* second);
void philo_start(table_t* table, unsigned int position);
void philo_stop(table_t* table, unsigned int position);

waiter_t* waiter_create(table_t* table);
int waiter_ask(waiter_t* w, philo_t* philo);
int waiter_release(waiter_t* w, philo_t* philo);

table_t* table_create(tablecfg_t* tcfg);
void table_destroy(table_t* table);
void table_start(table_t* table);
void table_stop(table_t* table);
philo_t* table_getPhilosopher(table_t* table, unsigned int position);
fork_t* table_getFork(table_t* table, unsigned int position);
waiter_t* table_getWaiter(table_t* table);

#endif

/** EOF **/

